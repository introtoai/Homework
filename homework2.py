from logic import *

coaches = ["Antonio", "Rodrigo", "Mykola"]
teams = ["Milan", "Real", "Metalist"]

symbols = []

knowledge = And()

for coach in coaches:
    for team in teams:
        symbols.append(Symbol(f"{coach}{team}"))

# Each coach belongs to a team.
for coach in coaches:
    knowledge.add(Or(
        Symbol(f"{coach}Milan"),
        Symbol(f"{coach}Real"),
        Symbol(f"{coach}Metalist")
    ))

# Only one team per coach.
for coach in coaches:
    for t1 in teams:
        for t2 in teams:
            if t1 != t2:
                knowledge.add(
                    Implication(Symbol(f"{coach}{t1}"), Not(Symbol(f"{coach}{t2}")))
                )

# Only one coach per team.
for team in teams:
    for c1 in coaches:
        for c2 in coaches:
            if c1 != c2:
                knowledge.add(
                    Implication(Symbol(f"{c1}{team}"), Not(Symbol(f"{c2}{team}")))
                )

# Coach and team cannot be of the same nationality
knowledge.add(
    Not(Symbol("MykolaMetalist"))
)
knowledge.add(
    Not(Symbol("RodrigoReal"))
)
knowledge.add(
    Not(Symbol("AntonioMilan"))
)

# а) Металіст не тренується в Антоніо. 
knowledgeA = knowledge
knowledgeA.add(
    Not(Symbol("AntonioMetalist"))
)

# б) Реал обіцяв ніколи не брати Миколу головним тренером.
knowledgeB = knowledge
knowledgeB.add(
    Not(Symbol("MykolaReal"))
)

print("A: Металіст не тренується в Антоніо. Тренер Металіста - ")
for symbol in symbols:
    if "Metalist" in str(symbol):
        if model_check(knowledgeA, symbol):
            print(str(symbol).replace('Metalist', ''))

print("Б: Реал обіцяв ніколи не брати Миколу головним тренером. Тренер Металіста - ")
for symbol in symbols:
    if "Metalist" in str(symbol):
        if model_check(knowledgeB, symbol):
            print(str(symbol).replace('Metalist', ''))
